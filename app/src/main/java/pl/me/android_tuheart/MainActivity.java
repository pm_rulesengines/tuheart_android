package pl.me.android_tuheart;

import android.app.Activity;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.Float;
import java.lang.Long;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import alice.tuprolog.*;


public class MainActivity extends Activity {
    private static final String PATH = Environment.getExternalStorageDirectory().getAbsolutePath()+"/tuheart/";
    private TextView textView;
    private static List<Long> memoryUsage = new LinkedList<>();
    private static List<Float> processorUsage = new LinkedList<>();
    private static boolean ifContinue = true;
    private Date start, end;
    private BufferedWriter bw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textview);
        textView.setKeepScreenOn(true);
    }

    public void go (View view) {
        /*
        Iniclalizacja atrybutow musi byc w pliku PL
         */
        try {
            bw = new BufferedWriter(new FileWriter(Environment.getExternalStorageDirectory()
                    .getAbsolutePath()+"/benchmark/tuheart.txt",true));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //measuring block
        ifContinue = true;
        new Monitor().execute();
        start = new Date();
        //measuring block

        Prolog engine = new Prolog();
        try{
            Theory t = new Theory(new java.io.FileInputStream(PATH+"tuheart.pl"));
            engine.setTheory(t);

//	     Theory thermostat = new Theory(new java.io.FileInputStream(srg[0]));
//	     engine.addTheory(thermostat);

            String [] arg = new String [2];
            arg[0] = PATH+"psc-zatrudnienie-ocena_kandydata.pl";
            arg[1] = "[ocenakandydata]";

//            arg[0] = "model_test.pl";
//            arg[1] = "[xschm_0, xschm_1, xschm_2]";

            Theory theory = new Theory(new java.io.FileInputStream(arg[0]));
            engine.addTheory(theory);

            System.out.println("Theory Loaded...");

            SolveInfo answer;

            answer = engine.solve("gox(input/1,"+ arg[1]+",foi).");
            answer = engine.solve("xstat current: State.");
            System.out.println(answer.getSolution());
            addTextToTextView(textView,answer.getSolution().toString());

            while(engine.hasOpenAlternatives()){
                answer = engine.solveNext();
                System.out.println(answer.getSolution());
                addTextToTextView(textView,answer.getSolution().toString());
            }
            end = new Date();
            ifContinue = false;

            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();

            textView.setKeepScreenOn(false);
        }catch(Exception e){e.printStackTrace();}
    }

    private String printDateTime() {
        Calendar c = Calendar.getInstance();
        return String.format("%tY-%tm-%td %tH:%tM:%tS : ", c, c, c, c, c, c);
    }

    public static void addTextToTextView(TextView t, String s) {
        t.setText(t.getText().toString() + "\n " + s);
    }

    private float readUsage() {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
            String load = reader.readLine();

            String[] toks = load.split(" +");  // Split on one or more spaces

            long idle1 = Long.parseLong(toks[4]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            try {
                Thread.sleep(360);
            } catch (Exception ignored) {}

            reader.seek(0);
            load = reader.readLine();
            reader.close();

            toks = load.split(" +");

            long idle2 = Long.parseLong(toks[4]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            return (float)(cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    public class Monitor extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (ifContinue) {
                memoryUsage.add(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
//                proc = readUsage();
//                System.out.println(proc);
                processorUsage.add(readUsage());
            }
            System.out.println("MONITOR: KONIEC");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Long [] mem = memoryUsage.toArray(new Long[memoryUsage.size()]);
            long minMem = mem[0];
            long maxMem = mem[0];
            for (long m : mem) {
                if (m < minMem) minMem = m;
                if (m > maxMem) maxMem = m;
            }

            Float[] proc = processorUsage.toArray(new Float[processorUsage.size()]);
            float minProc = proc[0];
            float maxProc = proc[0];
            for (float p : proc) {
                if (p < minProc && p >= 0) minProc = p;
                if (p > maxProc) maxProc = p;
            }
            try {
                bw.write(printDateTime());
                bw.write("Mem diff: ");
                bw.write(String.valueOf( (maxMem - minMem) / 1024L));
                bw.write(" KB , Processor diff: ");
                bw.write(String.valueOf( (maxProc-minProc)*100));
                bw.write("%, Time diff: ");
                bw.write(String.valueOf(end.getTime() - start.getTime()));
                bw.write(" ms \n-------\n");
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}
